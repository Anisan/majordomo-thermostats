import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import VueCircleSlider from 'vue-circle-slider'

import Vuetify from 'vuetify'
// index.js or main.js
import 'vuetify/dist/vuetify.min.css'

// initialize the vuex-i18 module
import vuexI18n from 'vuex-i18n';
Vue.use(vuexI18n.plugin, store);
// import predefined localizations
import translationsEn from './i18n/en.js';
import translationsRu from './i18n/ru.js';
// add translations
Vue.i18n.add('en', translationsEn);
Vue.i18n.add('ru', translationsRu);
// default locale is english
Vue.i18n.set('en');
store.state.lang = "en";


fetch('/ajax/thermostats.html?getConfig=1').then((response) => {
  return response.json().then((json) => {
    console.log('Config', json)
    Vue.i18n.set(json.lang);
    store.state.lang = json.lang;
    if (router.history.current.name == 'home')
      store.commit("setTitle", Vue.i18n.translate('thermostats'))
  })
});

//Vue.use(Vuetify)
Vue.use(Vuetify, {
  theme: {
    dark: {
      background: '#555555',
      primary: '#3f51b5',
      secondary: '#b0bec5',
      accent: '#8c9eff',
      error: '#b71c1c'
    }
  }
})
Vue.use(VueCircleSlider)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
import axios from 'axios';
export default new Vuex.Store({
  state: {
    device: {
      id : 0,
      normalTargetValue:0,
      ecoTargetValue:0,
      value:0
    },
    devices: [],
    updating: false,
    title:"",
    sheduler: {
      mo:{day:'week.mo',value:[0,144]},
      tu:{day:"week.tu",value:[0,144]},
      we:{day:"week.we",value:[0,144]},
      th:{day:"week.th",value:[0,144]},
      fr:{day:"week.fr",value:[0,144]},
      sa:{day:"week.sa",value:[0,144]},
      su:{day:"week.su",value:[0,144]},
    },
    lang: "",
  },
  getters:{
    allDevices: state => state.devices,
    loadData: state => state.updating,
    getTitle: state => state.title,
    getDevice: state => state.device,
    getSheduler: state => state.sheduler,
  },
  mutations: {
    setDevice(state, device) {
      let index = state.devices.findIndex(t => t.id === device.id)
      if (index >= 0)
        state.devices[index] = device
      else
        state.devices.push(device);
    },
    clearDevices(state) {
      state.devices = [];
    },
    setUpdating(state, data) {
      state.updating = data
    },
    setTitle(state, data) {
      state.title = data
    },
    setDeviceId(state, id) {
      let device = state.devices.find(t => t.id == id);
      if (device == null)
      {
        state.updating = true
        //state.devices = []
        state.devices.splice(0, state.devices.length)
        axios.get("/api.php/devices").then(response => {
          var dev_list = response.data.devices;
          for (var i = 0; i < dev_list.length; i++) {
            if (dev_list[i].type == "thermostat") {
              //console.log(dev_list[i])
              dev_list[i].normalTargetValue = parseInt(dev_list[i].normalTargetValue)
              dev_list[i].ecoTargetValue = parseInt(dev_list[i].ecoTargetValue)
              state.devices.push(dev_list[i]);
              if (dev_list[i].id == id)
                device = dev_list[i];
            }
          }
          state.title = device.title
          state.device = device;
          state.updating = false
        });
      }
      else{
        state.title = device.title
        state.device = device;
      }
    },
  },
  actions: {
    getAllDevices: function (context) {
      context.commit("setUpdating", true);
      context.commit("clearDevices");
      axios.get("/api.php/devices").then(response => {
        var dev_list = response.data.devices;
        //console.log(dev_list)
        for (var i = 0; i < dev_list.length; i++) {
          if (dev_list[i].type == "thermostat") {
            dev_list[i].normalTargetValue = parseInt(dev_list[i].normalTargetValue)
            dev_list[i].ecoTargetValue = parseInt(dev_list[i].ecoTargetValue)
            context.commit("setDevice", dev_list[i]);
          }
        }
        context.commit("setUpdating", false);
      });

    },
    loadSheduler: function(context, device){
      axios.get("/ajax/thermostats.html?getTaskShedule=1&device="+device).then(response => {
        var data = response.data;
        console.log(data);
        context.state.sheduler.mo.value = data[0];
        context.state.sheduler.tu.value = data[1];
        context.state.sheduler.we.value = data[2];
        context.state.sheduler.th.value = data[3];
        context.state.sheduler.fr.value = data[4];
        context.state.sheduler.sa.value = data[5];
        context.state.sheduler.su.value = data[6];

      });
    },
    saveDevice: function(context){
      let device = context.state.device
      let urlMethod = "/api.php/data/" + device.object+".normalTargetValue";
      axios.post(urlMethod, {data: device.normalTargetValue}).then(response => {
        console.log(response);
      });
      urlMethod = "/api.php/data/" + device.object+".ecoTargetValue";
      axios.post(urlMethod, {data: device.ecoTargetValue}).then(response => {
        console.log(response);
      });

      let shedule = [];
      shedule.push(context.state.sheduler.mo.value)
      shedule.push(context.state.sheduler.tu.value)
      shedule.push(context.state.sheduler.we.value)
      shedule.push(context.state.sheduler.th.value)
      shedule.push(context.state.sheduler.fr.value)
      shedule.push(context.state.sheduler.sa.value)
      shedule.push(context.state.sheduler.su.value)
      let data = JSON.stringify(shedule)
      //console.log(data)
      urlMethod = "/ajax/thermostats.html?setTaskShedule=1&device="+device.id+"&data="+data;
      axios.get(urlMethod).then(response => {
        var data = response.data;
        console.log(data);
      });
    },
    setGlobal: function (context, property) {
      let urlMethod = "/api.php/data/" + property.name;
      axios.post(urlMethod, {data: property.value}).then(response => {
        console.log(response);
      });
    },

  }
})
